package com.qms.controllers;

import com.qms.models.SystemUser;
import com.qms.services.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "users")
    public String userManagement(Model model) throws Exception {
        try {
            model.addAttribute("newUser", new SystemUser());
            model.addAttribute("users", userService.getUsers());
            model.addAttribute("roles", userService.getRoles());
            return "user/userManagement";
        } catch (Exception e) {
            e.printStackTrace();
            return "user/userManagement";
        }
    }

    @PostMapping(value = "users")
    public String newUser(@ModelAttribute SystemUser newUser) {
        userService.addUser(newUser);
        return "redirect:/users";
    }

    @GetMapping(value = "users/edit")
    public String editUserPage(@RequestParam int id, Model model) {
        SystemUser user = userService.getUserById(id);
        model.addAttribute("user", user);
        return "user/editUser";
    }


    @PostMapping(value = "users/edit")
    public String editUser(@ModelAttribute SystemUser user, Model model) {
        userService.editUser(user);
        return "redirect:/users";
    }

    @PostMapping(value = "users/add")
    public ResponseEntity<SystemUser> addUser(@RequestBody String data) {
        try {
            JSONObject user = new JSONObject(data);
            String username = user.getString("username");
            String password = user.getString("password");
            String roles = user.getString("roles");
            String endDate = user.getString("endDate");
            SystemUser newUser = new SystemUser();
            newUser.setUsername(username);
            newUser.setPassword(password);
            newUser.setRoles(roles);
            newUser.setEndDate(endDate);
            userService.addUser(newUser);
            return ResponseEntity.ok(newUser);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping(value = "login")
    public String loginPage(Model model) {
//        model.addAttribute("user", new SystemUser());
        return "user/login";
    }
//
//    @PostMapping(value = "/login")
//    public String login(@ModelAttribute SystemUser user, Model model) {
//        try {
//            userService.getUsers();
//            String username = user.getUsername();
//            String password = user.getPassword();
//            userService.login(username, password);
//            return "redirect:/users";
//        } catch (Exception e) {
//            model.addAttribute("user", new SystemUser());
//            model.addAttribute("error", e.getMessage());
//            return "login";
//        }
//    }
//
//    @GetMapping(value = "/logout")
//    public String logout() {
//        userService.logout();
//        return "redirect:/login";
//    }
}
