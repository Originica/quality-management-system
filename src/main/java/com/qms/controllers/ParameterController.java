package com.qms.controllers;

import com.qms.models.QMSParams;
import com.qms.services.ParameterService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

@Controller
@RequestMapping("")
public class ParameterController {

    @Autowired
    ParameterService parameterService;

    @RequestMapping(method = RequestMethod.GET, value = "/parameters")
    public String parameters() {
        return "parameters/parameters";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getParameters", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<QMSParams>> getAllParameters() {
        List<QMSParams> result = new ArrayList<>();
        try {
            result = parameterService.getParameters();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/parametersByType", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<QMSParams>> getParameters(@RequestBody String type) {
        List<QMSParams> result = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(type);
            result = parameterService.getParameters(data.getString("type"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/parameters/add")
    public ResponseEntity<QMSParams> addParameters(@RequestBody String parameter) {
        try {
            JSONObject newParameter = new JSONObject(parameter);
            String specCode = newParameter.getString("qms_spec_code");
            String valueName = newParameter.getString("qms_value_name");
            String paramValue = newParameter.getString("qms_param_value");
            String method = newParameter.getString("qms_method");
            int type = newParameter.getInt("qms_type");
            QMSParams paramObject = new QMSParams(specCode, type, valueName, paramValue, method);
            parameterService.createParameters(paramObject);
            return ResponseEntity.ok(paramObject);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/parameters/update")
    public ResponseEntity<QMSParams> updateParameters(@RequestBody String parameter) {
        JSONObject newParameter = new JSONObject(parameter);
        String specCode = newParameter.getString("qms_spec_code");
        String valueName = newParameter.getString("qms_value_name");
        String paramValue = newParameter.getString("qms_param_value");
        String method = newParameter.getString("qms_method");
        int type = 0;
        QMSParams newParameterObject = new QMSParams(specCode, type, valueName, paramValue, method);
        try {
            parameterService.updateParameters(newParameterObject);
            return ResponseEntity.ok(newParameterObject);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/parameters/delete")
    public ResponseEntity deleteParameters(@RequestBody String parameter) {
        JSONObject param = new JSONObject(parameter);
        String specCode = param.getString("qms_spec_code");
        try {
            parameterService.deleteParameters(specCode);
            return ResponseEntity.ok("");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
