package com.qms.controllers;

import com.qms.models.QMSInspectionHis;
import com.qms.models.QMSParams;
import com.qms.models.Query;
import com.qms.models.Summary;
import com.qms.services.InspectionService;
import com.qms.services.ParameterService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/inspection")
public class    InspectionController {

    @Autowired
    ParameterService parameterService;

    @Autowired
    InspectionService inspectionService;

    @RequestMapping(method = RequestMethod.GET, value = "")
    public String inspection() {
        return "inspection/inspection";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/get")
    public ResponseEntity<Iterable<QMSInspectionHis>> getInspections() {
        try {
            List<QMSInspectionHis> qmsInspectionHisList = inspectionService.getInspections();
            return ResponseEntity.ok(qmsInspectionHisList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/get/id")
    public ResponseEntity<Iterable<Integer>> getInspectionsId() {
        try {
            List<Integer> qmsInspectionHisList = inspectionService.getItemId();
            return ResponseEntity.ok(qmsInspectionHisList);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/get/item")
    public ResponseEntity<Iterable<QMSInspectionHis>> getInspectionItemById(@RequestBody String data) {
        try {
            JSONObject id = new JSONObject(data);
            List <QMSInspectionHis> qmsInspectionHis = inspectionService.getInspectionsByItemId(id.getInt("itemId"));
            return ResponseEntity.ok(qmsInspectionHis);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public ResponseEntity<QMSInspectionHis> updateInspection(@RequestBody String inspection) {
        JSONObject newInspection = new JSONObject(inspection);
        int id = newInspection.getInt("qms_inspect_id");
        String result = newInspection.getString("qms_result");
        QMSInspectionHis newInspectionObject = new QMSInspectionHis();
        newInspectionObject.setQmsInspectID(id);
        newInspectionObject.setResult(result);
        try {
            inspectionService.updateInspections(newInspectionObject);
            return ResponseEntity.ok(newInspectionObject);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public ResponseEntity deleteInspection(@RequestBody String inspection) {
        JSONObject item = new JSONObject(inspection);
        String id = item.getString("qms_inspect_id");
        try {
            inspectionService.deleteInspections(id);
            return ResponseEntity.ok("");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/reporting")
    public String reporting(Model model) {
        try {
            Query query = new Query();
            model.addAttribute("query", query);
            return "inspection/reporting";
        } catch (Exception e) {
            e.printStackTrace();
            return "inspection/reporting";
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/reporting")
    public String reporting(@ModelAttribute Query query, Model model) {
        try {
            List<Summary> summaries = inspectionService.getSummary(query);
            List<QMSInspectionHis> result = inspectionService.query(query);
            model.addAttribute("summaries", summaries);
            model.addAttribute("report", result);
            return "inspection/reporting";
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("error", e.getMessage());
            return "inspection/reporting";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/export")
    public String exportInspection(@RequestParam int batchId, Model model) {
        model.addAttribute("batchId", batchId);
        model.addAttribute("dateToday", LocalDate.now());
        model.addAttribute("params", parameterService.getParameters());
        model.addAttribute("items", inspectionService.getInspectionsByBatchId(batchId));
        return "inspection/export";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/export/all")
    public String exportAllInspections(Model model) {
        model.addAttribute("dateToday", LocalDate.now());
        model.addAttribute("params", parameterService.getParameters());
        model.addAttribute("items", inspectionService.getInspections());
        return "inspection/export";
    }
}
