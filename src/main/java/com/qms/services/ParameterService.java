package com.qms.services;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qms.models.QMSParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ParameterService {

    @Autowired
    ConnectionService connectionService;

    public void createParameters(QMSParams qmsParams) {
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "INSERT INTO QMS_PARAMS" +
                    String.format(" VALUES ('%s', '%s', '%s', '%s', '%s')",
                            qmsParams.getSpecCode(), qmsParams.getTypes(),
                            qmsParams.getValueName(), qmsParams.getParamValue(), qmsParams.getMethod());
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage());
        }
    }

    public List<QMSParams> getParameters(){
        ResultSet resultSet = null;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "SELECT * FROM QMS_PARAMS";
            resultSet = statement.executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        ArrayList<QMSParams> result = resultSetToObjectList(resultSet);
        return result;
    }

    public List<QMSParams> getParameters(String type) throws Exception {
        ResultSet resultSet = null;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM QMS_PARAMS" +
                    String.format(" WHERE qms_type = %s ORDER BY qms_type ASC", type);
            resultSet = statement.executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<QMSParams> result = resultSetToObjectList(resultSet);
        return result;
    }

    public void updateParameters(QMSParams qmsParams) throws Exception {
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "UPDATE QMS_PARAMS" +
                    String.format(" SET qms_value_name = '%s', qms_param_value = '%s', qms_method = '%s'",
                            qmsParams.getValueName(), qmsParams.getParamValue(), qmsParams.getMethod())
                    + String.format(" WHERE qms_spec_code = '%s'", qmsParams.getSpecCode());
            statement.executeUpdate(sql);
            statement.close();
        }   catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteParameters(String specCode) throws Exception {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "DELETE FROM QMS_PARAMS" +
                    String.format(" WHERE qms_spec_code = '%s'", specCode);
            statement.executeUpdate(sql);
            statement.close();
        }   catch (Exception e){
            throw e;
        }
    }

    public List<String> getParameterTypes() throws Exception {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = String.format("SELECT DISTINCT qms_type FROM QMS_PARAMS");
            resultSet = statement.executeQuery(sql);
        }   catch (Exception e){
            throw e;
        }
        List<String> list = new ArrayList<String>();
        while (resultSet.next()) {
            list.add(resultSet.getString("qms_type"));
        }
        return list;
    }

    public static JSONArray resultSetToJson(ResultSet resultSet) throws Exception {
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()) {
            JSONObject object = new JSONObject();
            int total_rows = resultSet.getMetaData().getColumnCount();
            for (int i = 0; i < total_rows; i++) {
                object.put(resultSet.getMetaData().getColumnLabel(i+1).toLowerCase(), resultSet.getObject(i+1));
            }
            jsonArray.put(object);
        }
        return jsonArray;
    }

    public static List<String> resultSetToList(ResultSet resultSet) throws Exception {
        List<String> list = new ArrayList<String>();
        while (resultSet.next()) {
            list.add(resultSet.getString("qms_type"));
        }
        return list;
    }

    public static ArrayList<QMSParams> resultSetToObjectList(ResultSet resultSet) {
        ArrayList<QMSParams> result = new ArrayList<QMSParams>();
        try {
            while (resultSet.next()) {
                QMSParams qmsParams = new QMSParams();
                qmsParams.setSpecCode(resultSet.getString("qms_spec_code"));
                qmsParams.setTypes(resultSet.getInt("qms_type"));
                qmsParams.setValueName(resultSet.getString("qms_value_name"));
                qmsParams.setParamValue(resultSet.getString("qms_param_value"));
                qmsParams.setMethod(resultSet.getString("qms_method"));
                result.add(qmsParams);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
