package com.qms.services;

import com.qms.models.QMSParams;
import com.qms.models.QMSRole;
import com.qms.models.SystemUser;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Service
public class UserService implements UserDetailsService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    ConnectionService connectionService;

    public List<SystemUser> getUsers() {
        ResultSet resultSet = null;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM SYSTEM_USER ORDER BY staff_id ASC";
            resultSet = statement.executeQuery(sql);
            ArrayList<SystemUser> result = new ArrayList<SystemUser>();
            while (resultSet.next()) {
                SystemUser user = new SystemUser();
                user.setStaffId(resultSet.getString("staff_id"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setUsername(resultSet.getString("sys_user"));
                user.setName(resultSet.getString("staff_name"));
                user.setEffectiveDate(resultSet.getString("eff_date"));
                user.setEndDate(resultSet.getString("end_date"));
                String role = getRoleById(resultSet.getInt("role_id")).getName();
                user.setRoles(role);
                result.add(user);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public SystemUser getUserById(int id) {
        ResultSet resultSet = null;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM SYSTEM_USER" +
                    String.format(" WHERE staff_id = %s", id);
            resultSet = statement.executeQuery(sql);
            SystemUser user = new SystemUser();
            while (resultSet.next()) {
                user.setStaffId(resultSet.getString("staff_id"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setUsername(resultSet.getString("sys_user"));
                user.setName(resultSet.getString("staff_name"));
                user.setEffectiveDate(resultSet.getString("eff_date"));
                user.setEndDate(resultSet.getString("end_date"));
                String role = getRoleById(resultSet.getInt("role_id")).getName();
                user.setRoles(role);
            }
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public QMSRole getRoleById(int id) throws Exception {
        ResultSet resultSet = null;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM QMS_ROLE" +
                    String.format(" WHERE role_id = %s", id);
            resultSet = statement.executeQuery(sql);
            QMSRole result = new QMSRole();
            while (resultSet.next()) {
                result.setId(id);
                result.setName(resultSet.getString("role_name"));
                result.setDescription(resultSet.getString("role_description"));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<QMSRole> getRoles() throws Exception {
        ResultSet resultSet = null;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM QMS_ROLE";
            resultSet = statement.executeQuery(sql);
            List<QMSRole> result = new ArrayList<QMSRole>();
            while (resultSet.next()) {
                QMSRole qmsRole = new QMSRole();
                qmsRole.setId(resultSet.getInt("role_id"));
                qmsRole.setName(resultSet.getString("role_name"));
                qmsRole.setDescription(resultSet.getString("role_description"));
                result.add(qmsRole);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getStaffName(int staffId) {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "SELECT staff_name FROM SYSTEM_USER " +
                    String.format(" WHERE staff_id = %s", staffId);
            resultSet = statement.executeQuery(sql);
            resultSet.next();
            String result = resultSet.getString("staff_name");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public SystemUser addUser(SystemUser newUser) {
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT staff_id from SYSTEM_USER ORDER BY staff_id DESC LIMIT 1";
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            String password = passwordEncoder.encode(newUser.getPassword());
            int id = resultSet.getInt("staff_id") + 1;
            sql = "INSERT INTO SYSTEM_USER (staff_id, sys_user, password, eff_date, end_date, role_id)" +
                    String.format(" VALUES (%s, '%s', '%s', '%s', '%s', %s)",
                            id, newUser.getUsername(), password, LocalDate.now(), newUser.getEndDate(), newUser.getRoles());
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return newUser;
    }

    public void editUser(SystemUser user) {
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "UPDATE SYSTEM_USER" +
                    String.format(" SET staff_name = '%s', email = '%s'", user.getName(), user.getEmail());
            System.out.println(user.getPassword());
            if (user.getPassword() != null && !user.getPassword().isEmpty()) {
                String password = passwordEncoder.encode(user.getPassword());
                sql = sql + String.format(", password = '%s'", password);
            }
            if (user.getEndDate() != null && !user.getEndDate().isEmpty()) {
                sql = sql + String.format(", end_date = '%s'", user.getEndDate());
            }
            sql = sql + String.format(" WHERE staff_id = %s", user.getStaffId());
            System.out.println(sql);
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public SystemUser login(String username, String password) throws Exception {
//        for (SystemUser systemUser : systemUsers ) {
//            if (username.equals(systemUser.getUsername())) {
//                if (password.equals(systemUser.getPassword())) {
//                    currentUser = systemUser;
//                    return systemUser;
//                }
//                throw new Exception("Invalid password");
//            }
//        }
//        throw new Exception("Username not found");
//    }

//    public void logout() {
//        currentUser = null;
//    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<SystemUser> systemUsers = getUsers();
        for (SystemUser systemUser : systemUsers ) {
            if (username.equals(systemUser.getUsername())) {
                return systemUser;
            }
        }
        throw new UsernameNotFoundException(String.format("Username %s is not found", username));
    }
}
