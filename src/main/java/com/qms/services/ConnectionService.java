package com.qms.services;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;

@Service
public class ConnectionService {

    private static Connection connection;

    private String host;
    private int port;
    private String username;
    private String password;
    private String dbname;

    @PostConstruct
    public void setConnection() throws Exception {

        host = "ec2-23-23-164-251.compute-1.amazonaws.com";
        port = 5432;
        username = "idmmtxlwosntzx";
        password = "c31832d6a16851dd4b4240f1c5b2348a73c93ef0382490219079d8fc9f473054";
        dbname = "d62f8jsvk6anss";

        String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbname;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(true);
        } catch (Exception e) {
            throw e;
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
