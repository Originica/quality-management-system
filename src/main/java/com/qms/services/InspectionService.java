package com.qms.services;

import com.qms.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class InspectionService {

    @Autowired
    ConnectionService connectionService;

    public List<QMSInspectionHis> getInspections() {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "SELECT * FROM QMS_INSPECT_HIS NATURAL JOIN QMS_PARAMS";
            resultSet = statement.executeQuery(sql);
            List<QMSInspectionHis> result = resultSetToObjectList(resultSet);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Integer> getItemId() throws Exception {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "SELECT DISTINCT item_id FROM QMS_INSPECT_HIS";
            resultSet = statement.executeQuery(sql);
            List<Integer> result = resultSetToList(resultSet);
            return result;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<QMSInspectionHis> getInspectionsByItemId(int id) {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "SELECT * FROM QMS_INSPECT_HIS NATURAL JOIN QMS_PARAMS" +
                    String.format(" WHERE item_id = %s ORDER BY qms_inspect_id ASC", id);
            resultSet = statement.executeQuery(sql);
            List<QMSInspectionHis> result = resultSetToObjectList(resultSet);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<QMSInspectionHis> getInspectionsByBatchId(int id) {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "SELECT * FROM QMS_INSPECT_HIS NATURAL JOIN QMS_PARAMS" +
                    String.format(" WHERE batch_id = %s", id);
            resultSet = statement.executeQuery(sql);
            List<QMSInspectionHis> result = resultSetToObjectList(resultSet);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateInspections(QMSInspectionHis qmsInspectionHis) throws Exception {
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();

            String sql = "UPDATE QMS_INSPECT_HIS" +
                    String.format(" SET result = '%s'", qmsInspectionHis.getResult())
                    + String.format(" WHERE qms_inspect_id = %s", qmsInspectionHis.getQmsInspectID());
            statement.executeUpdate(sql);
            statement.close();
        }   catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deleteInspections(String id) throws Exception {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "DELETE FROM QMS_INSPECT_HIS" +
                    String.format(" WHERE qms_inspect_id = %s", id);
            statement.executeUpdate(sql);
            statement.close();
        }   catch (Exception e){
            throw e;
        }
    }

    public static List<QMSInspectionHis> resultSetToObjectList(ResultSet resultSet) throws Exception {
        List<QMSInspectionHis> result = new ArrayList<QMSInspectionHis>();
        while (resultSet.next()) {
            QMSInspectionHis qmsInspectionHis = new QMSInspectionHis();
            qmsInspectionHis.setQmsInspectID(resultSet.getInt("qms_inspect_id"));
            qmsInspectionHis.setStaffId(resultSet.getInt("staff_id"));
            qmsInspectionHis.setSpecCode(resultSet.getString("qms_spec_code"));
            qmsInspectionHis.setStateDate(resultSet.getString("state_date"));
            qmsInspectionHis.setItemId(resultSet.getInt("item_id"));
            qmsInspectionHis.setBatchId(resultSet.getInt("batch_id"));
            qmsInspectionHis.setResult(resultSet.getString("result"));
            qmsInspectionHis.setInspectRes(resultSet.getDouble("qms_inspect_res"));
            qmsInspectionHis.setValueName(resultSet.getString("qms_value_name"));
            qmsInspectionHis.setParamValue(resultSet.getString("qms_param_value"));
            qmsInspectionHis.setEquipment(resultSet.getString("qms_method"));
            qmsInspectionHis.setStandard(resultSet.getString("standard"));
            qmsInspectionHis.setTypes(resultSet.getInt("qms_type"));
            result.add(qmsInspectionHis);
        }
        return result;
    }

    public List<QMSInspectionHis> query(Query query) {
        ResultSet resultSet;
        try {
            Connection connection = connectionService.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM QMS_INSPECT_HIS NATURAL JOIN QMS_PARAMS" +
                    String.format(" WHERE STATE_DATE >= '%s' AND STATE_DATE <= '%s'", query.getStartDate(), query.getEndDate());
            if (query.getItemId() != 0) {
                sql = sql + String.format(" AND ITEM_ID = %s", query.getItemId());
            }
            resultSet = statement.executeQuery(sql);
            List<QMSInspectionHis> result = resultSetToObjectList(resultSet);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Summary> getSummary(Query query) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = new Date();
        Date endDate = new Date();
        try {
            startDate = simpleDateFormat.parse(query.getStartDate());
            endDate = simpleDateFormat.parse(query.getEndDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        endDate = addOneDay(endDate);
        int itemId = query.getItemId();
        List<Summary> summaries = new ArrayList<Summary>();
        while (startDate.before(endDate)) {
            ResultSet resultSet;
            Summary summary = new Summary();
            summary.setDate(simpleDateFormat.format(startDate));
            try {
                Connection connection = connectionService.getConnection();
                Statement statement = connection.createStatement();
                String sql = "SELECT COUNT(*) FROM QMS_INSPECT_HIS" +
                        String.format(" WHERE STATE_DATE = '%s'", simpleDateFormat.format(startDate));
                if (itemId != 0) {
                    sql = sql + String.format(" AND ITEM_ID = %s", itemId);
                }
                resultSet = statement.executeQuery(sql);
                resultSet.next();
                int total = resultSet.getInt("count");
                if (total != 0) {
                    sql = sql + " AND QMS_INSPECT_RES = 'OK'";
                    resultSet = statement.executeQuery(sql);
                    resultSet.next();
                    summary.setOk(resultSet.getInt("count"));
                    summary.setNg(total - summary.getOk());
                    DecimalFormat decimalFormat = new DecimalFormat("##.####%");
                    double deflectRate = summary.getNg()/summary.getOk();
                    summary.setRate(decimalFormat.format(deflectRate));
                    summaries.add(summary);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            startDate = addOneDay(startDate);
        }
        return summaries;
    }

    public static List<Integer> resultSetToList(ResultSet resultSet) throws Exception {
        List<Integer> result = new ArrayList<Integer>();
        while (resultSet.next()) {
            result.add(resultSet.getInt("item_id"));
        }
        return result;
    }

    public Date addOneDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }
}
