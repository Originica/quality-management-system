package com.qms.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QMSParams {

    private String specCode;
    private int types;
    private String valueName;
    private String paramValue;
    private String method;
//    private String standard;

}
