package com.qms.models;

import com.qms.services.ConnectionService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QMSInspectionHis {

    private int qmsInspectID;
    private int staffId;
    private String specCode;
    private String stateDate;
    private int batchId;
    private int itemId;
    private double inspectRes;
    private String result;
    private int types;
    private String valueName;
    private String paramValue;
    private String standard;
    private String equipment;
}
