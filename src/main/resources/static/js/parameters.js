$(document).ready(function(){
    $('#typeDropdown').change(function(){
        load();
    })

    $("#newParam").click(function(event){
        event.preventDefault();
        submitForm();
        });

});
$(document).ajaxComplete(function(){
    $(':button').on("click", function(event) {
        var form = $(this).parents('.param:first');
        if($(this).attr("name") == "update") {
            updateParams(form);
        } else  if($(this).attr("name") == "delete"){
            deleteParams(form);
        }
    });
})

function load() {
    var type = $('#typeDropdown :selected').val();
    var data = {};
    data['type'] = type;
    data = JSON.stringify(data)

    $.ajax({
        url: "/parametersByType",
        data: data,
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        success: function(result) {

            $("#paramsList").empty();

            for (i = 0; i < result.length; i++){
                var item = i + 1;
                var specCode = result[i].specCode;
                var valueName = result[i].valueName;
                var paramValue = result[i].paramValue;
                var method = result[i].method;

                $("#paramsList").append(
                '<h6 class="mt-4">Inspection Item ' + item + ':</h6>' +
                '<form class="param">' +
                    '<div class="form-group row">' +
                        '<input type="hidden" value="' + specCode + '" class="code" name="qms_spec_code">' +
                    '</div>' +
                    '<div class="form-group row">' +
                        '<div class="col-sm-2">' +
                            '<label for="qms_value_name" class="col-form-label">Inspection Name</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + valueName + '" class="name" name="qms_value_name" required>' +
                        '</div>'+
                    '</div>' +
                    '<div class="form-group row">' +
                        '<div class="col-sm-2">' +
                            '<label for="value" class="col-form-label">Inspection Std Value</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + paramValue + '" class="value" name="qms_param_value" required>' +
                        '</div>'+
                    '</div>' +
                    '<div class="form-group row">' +
                        '<div class="col-sm-2">' +
                            '<label for="method" class="col-form-label">Inspection Method</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + method + '" class="method" name="qms_method" required>' +
                        '</div>'+
                    '</div>' +
                    '<div class="my-2">' +
                        '<input type="button" name="update" value="Update" class="btn btn-primary btn-medium mx-2">' +
                        '<input type="button" name="delete" value="Delete" class="btn btn-danger btn-medium mx-2">' +
                    '<div>' +
                '</form>'
                )
            }
        },
        error: function(result) {
       }
    });
};

function submitForm(){
    newParameter = {};

    var type = $("#addParamsForm").find(":selected").text();

    newParameter['qms_type'] = $("#addParamsForm #type").val();
    newParameter['qms_spec_code'] = $("#addParamsForm #code").val();
    newParameter['qms_value_name'] = $("#addParamsForm #name").val();
    newParameter['qms_param_value'] = $("#addParamsForm #value").val();
    newParameter['qms_method'] = $("#addParamsForm #method").val();
    newParameter = JSON.stringify(newParameter);
    console.log(newParameter);

    $.ajax({
        url: "/parameters/add",
        data: newParameter,
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        success: function(result){
            $(".modal").modal('hide');
            alert("Data added.")
            load();
            },
        error: function(result){
            alert("error")
            }
        });
};

function updateParams(form){
    parameter = {};
    parameter['qms_spec_code'] = $(" .code", form).val();
    parameter['qms_value_name'] = $(" .name", form).val();
    parameter['qms_param_value'] = $(" .value", form).val();
    parameter['qms_method'] = $(" .method", form).val();
    parameter = JSON.stringify(parameter);

    $.ajax({
        url: "/parameters/update",
        data: parameter,
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        success: function(result){
            alert("Data updated.")
            },
        error: function(result){
            alert("error")
        }
    });
 }

function deleteParams(form) {
    parameter = {};
    parameter['qms_spec_code'] = $(" .code", form).val();
    parameter = JSON.stringify(parameter);
        $.ajax({
            url: "/parameters/delete",
            data: parameter,
            dataType: "json",
            contentType: "application/json",
            type: "POST",
            success: function(){
                alert("Data deleted.");
                load();
                },
            error: function(){
                console.log("error");
            }
        });
}
