$(document).ready(function(){
    $('#batch').hide();

    $('#itemId').change(function(){
        var item = $('#itemId').val();
        var items = getInspectionItem(item);
        var batches = new Set();

        for ( i = 0; i < items.length; i++ ) {
            batches.add(items[i].batchId);
        }
        batches = Array.from(batches).sort();
        $('#batchId').empty();
        $('#batchId').append('<option selected disabled value="">Select batch...</option>');

        for ( i = 0; i < batches.length; i++ ) {
            $('#batchId').append(
            '<option value="' + batches[i] + '">' + batches[i] +'</option>'
            );
        }

        $('#batch').show();

        $('#batchId').change(function(){
            var batch = $('#batchId :selected').val();
            loadItem(items);
        });
    });
})

function loadItem(items) {
    $('#inspectionItemsList').empty();

    if ( items.length == 0 ) {
        $('#inspectionItemsList').append('<p>No item found.</p>')
    } else {
        $('#inspectionItemsList').append(
            '<div class="row g-3 align-items-center">' +
                '<div class="col-sm-2"' +
                    '<label for="code" class="col-sm-3 col-form-label">Inspection Type</label>' +
                '</div>' +
                 '<div class="col-auto">' +
                     '<select name="type" id="typeDropdown">' +
                             '<option selected disabled value="">Select parameter type...</option>' +
                         '<option value="1">Type 1</option>' +
                         '<option value="2">Type 2</option>' +
                         '<option value="3">Type 3</option>' +
                    '</select>' +
                '</div>' +
            '</div>' +
            '<div id="itemDropdown"></div>' +
            '<div class="container" id="itemsList">'+'</div>'
        );

        $('body').delegate("#typeDropdown", 'change', function(event) {
            loadItemByType(items);
        });
    }

}

function loadItemFromDropdown() {
    var inspections = getInspections();
    $('#itemDropdown').empty();

    for ( i = 0; i < inspections.length; i++ ) {
        $('#itemDropdown').append(
            '<option value="' + inspections[i] + '">' + inspections[i] + '</option>'
        );
    };
    loadItem(inspections[item]);
}


function loadItemByType(items) {
    var type = $('#typeDropdown :selected').val();
    var batch = $('#batchId :selected').val();
    $('#itemDropdown').empty();
    $("#itemsList").empty();
    count = 1;
    $('#itemDropdown').append('<select name="item" id="itemOptions">');
    for ( i = 0; i < items.length; i++ ) {
        if ( items[i].types == type && items[i].batchId == batch) {

            var id = items[i].qmsInspectID;
            var specCode = items[i].specCode;
            var valueName = items[i].valueName;
            var standard = items[i].standard;
            var paramValue = items[i].paramValue;
            var result = items[i].result;
            var inspectRes = items[i].inspectRes;
            var method = items[i].equipment;

            $('#itemOptions').append('<option value="item' + i + '">Item' + count + '</option>');

            $("#inspectionItemsList").append(
                '<div class="item" id="item' + i + '">' +
                '<h6 class="mt-4">Inspection Item ' + count + ':</h6>' +
                '<form class="inspection">' +
                    '<div class="form-group row">' +
                        '<input type="hidden" value="' + specCode + '" class="code" name="qms_spec_code">' +
                    '</div>' +
                    '<div class="form-group row">' +
                        '<input type="hidden" value="' + id + '" class="id" name="id">' +
                    '</div>' +
                    '<div class="row g-3 align-items-center">' +
                        '<div class="col-sm-2">' +
                            '<label for="qms_value_name" class="col-form-label">Inspection Name</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="readonly" value="' + valueName + '" class="name" name="qms_value_name" required>' +
                        '</div>' +
                    '</div>' +
//                    '<div class="row g-3 align-items-center">' +
//                        '<div class="col-sm-2">' +
//                            '<label for="value" class="col-form-label">Inspection Std Value</label>' +
//                        '</div>' +
//                        '<div class="col-sm-3">' +
//                            '<input type="text" value="' + paramValue + '" class="value" name="qms_param_value" required>' +
//                        '</div>' +
//                    '</div>' +
                    '<div class="row g-3 align-items-center">' +
                        '<div class="col-sm-2">' +
                            '<label for="value" class="col-form-label">Inspection Standard</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + standard + '" class="standard" name="standard" required>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row g-3 align-items-center">' +
                        '<div class="col-sm-2">' +
                            '<label for="value" class="col-form-label">Inspection Result</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + inspectRes + '" class="standard" name="inspectRes" required>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row g-3 align-items-center">' +
                        '<div class="col-sm-2">' +
                            '<label for="result" class="col-form-label">Result</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + result + '" class="result" name="result" required>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row g-3 align-items-center">' +
                        '<div class="col-sm-2">' +
                            '<label for="method" class="col-form-label">Equipment</label>' +
                        '</div>' +
                        '<div class="col-sm-3">' +
                            '<input type="text" value="' + method + '" class="method" name="qms_method" required>' +
                        '</div>' +
                    '</div>' +
                    '<div class="my-2">' +
                        '<input type="button" name="update" value="Update" class="btn btn-primary btn-medium mx-2">' +
                        '<input type="button" name="delete" value="Delete" class="btn btn-danger btn-medium mx-2">' +
                    '</div>' +
                    '</div>' +
                '</form>'
            )
                count = count + 1;
        }
    }
    $('#itemDropdown').append('</select>');
    $('body').delegate('#itemDropdown', 'change', function(event) {
        showItem();
    });
    $(':button').on("click", function(event) {
        var form = $(this).parents('.inspection:first');
        if($(this).attr("name") == "update") {
            updateInspectionItem(form);
        } else  if($(this).attr("name") == "delete"){
            deleteInspection(form);
        }
    });
    showItem();
}

function showItem() {
    var item = $('#itemOptions :selected').val();
    $('.item').hide();
    $('#'+item).show();

}


function getInspections() {
    var inspections = null;
    $.ajax({
            async: false,
            url: "/inspection/get/id",
            dataType: "json",
            contentType: "application/json",
            type: "POST",
            success: function(result) {
                inspections = result;
            },
        });
                return inspections;
}

function getInspectionItem(item) {
    var items = null;
    var data = {};
    data['itemId'] = item;
    data = JSON.stringify(data);
    $.ajax({
        async: false,
        url: "/inspection/get/item",
        data: data,
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        success: function(result) {
            items = result;
        },
    });
    return items;
}

function updateInspectionItem(form){
    inspection = {};
    inspection['qms_inspect_id'] = $(" .id", form).val();
    inspection['qms_result'] = $(" .result", form).val();
    inspection = JSON.stringify(inspection);

    $.ajax({
        url: "/inspection/update",
        data: inspection,
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        success: function(result){
            alert("Data updated.")
            },
        error: function(result){
            alert("error")
        }
    });
 }

function deleteInspection(form) {
    inspection = {};
    inspection['qms_inspect_id'] = $(" .id", form).val();
    inspection = JSON.stringify(inspection);

        $.ajax({
            url: "/inspection/delete",
            data: inspection,
            dataType: "json",
            contentType: "application/json",
            type: "POST",
            success: function(){
                alert("Data deleted.");
                },
            error: function(){
                console.log("error");
            }
        });
}