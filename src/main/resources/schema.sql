CREATE TABLE IF NOT EXISTS QMS_ROLE (
    role_id INTEGER NOT NULL,
    role_name VARCHAR(250) UNIQUE NOT NULL,
    role_description VARCHAR(1000),
    PRIMARY KEY (role_id)
);

CREATE TABLE IF NOT EXISTS SYSTEM_USER (
    staff_id INTEGER,
    sys_user VARCHAR(250) UNIQUE NOT NULL,
    password VARCHAR(250) NOT NULL,
    email VARCHAR(250),
    staff_name VARCHAR(250),
    status VARCHAR(250),
    eff_date DATE,
    end_date DATE,
    role_id INTEGER,
    PRIMARY KEY (staff_id),
    FOREIGN KEY (role_id) REFERENCES QMS_ROLE (role_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS QMS_PARAMS (
    qms_spec_code VARCHAR(100) UNIQUE,
    qms_type INTEGER NOT NULL,
    qms_value_name VARCHAR(128) NOT NULL,
    qms_param_value VARCHAR(128) NOT NULL,
    qms_method VARCHAR(128) NOT NULL,
    PRIMARY KEY (qms_spec_code)
);

CREATE TABLE IF NOT EXISTS QMS_INSPECT_HIS (
    qms_inspect_id SERIAL,
    staff_id INTEGER,
    qms_spec_code VARCHAR(100),
    qms_inspect_res VARCHAR(250),
    state_date DATE,
    batch_id INTEGER,
    item_id INTEGER,
    PRIMARY KEY (qms_inspect_id),
    FOREIGN KEY (staff_id) REFERENCES SYSTEM_USER (staff_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (qms_spec_code) REFERENCES QMS_PARAMS(qms_spec_code) ON UPDATE CASCADE ON DELETE CASCADE
);